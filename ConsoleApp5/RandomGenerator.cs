﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp5
{
    class RandomGenerator
    {
        private static RandomGenerator instance;
        private double[,] matrix;
        private int p, k;

        
        private Random generator = new Random();



        public static RandomGenerator GetInstance()
        {
            if (instance == null)
            {
                instance = new RandomGenerator();
            }
            return instance;
        }


        private RandomGenerator()
        {

            this.matrix = new double[1, 1];

        }


        public void Set(int p, int k)
        {
            this.p = p;
            this.k = k;
            this.matrix = new double[p, k];
        }
        public void fillMatrix()
        {
            for (int i = 0; i < p; i++)
            {
                for (int j = 0; j < k; j++)
                {
                    this.matrix[i, j] = generator.NextDouble();
                }
            }
        }



        public void printMatrix()
        {
            for (int i = 0; i < p; i++)
            {
                Console.WriteLine();
                for (int j = 0; j < k; j++)
                {
                    Console.Write(matrix[i, j] + " ");
                }
            }
            Console.WriteLine();
        }




       
    }
}

