﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp5
{
    class Program
    {
        static void Main(string[] args)
        {
            //ZADATAK 1
            Dataset p1 = new Dataset("file.txt");
            Dataset p2 = (Dataset)p1.Clone();

            p1.Print();
            p2.Print();
            //"Odgovor na pitanje: Duboko kopiranje koristimo ako želimo klonu mijenjati vrijednosti a da pritom orginal ostane nepromijenjen, u suprotnom koristimo plitko!"

            //ZADATAK 2
            RandomGenerator M = RandomGenerator.GetInstance();
            M.Set(4, 2);
            M.fillMatrix();
            M.printMatrix();


            //ZADATAK 3
            Logger P = Logger.GetInstance();
            P.Log("bilosta");

            //ZADATAK 4
            ConsoleNotification Text = new ConsoleNotification("Marko", "Neki naslov", "Neki text", DateTime.Now, Category.INFO, ConsoleColor.Green);
            NotificationManager Builder = new NotificationManager();
            Builder.Display(Text);

        }
    }
}
